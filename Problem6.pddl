;
; Lewis Wilson, AI coursework 
; LW52 - H00270128
;
(define (problem Problem6)
  (:domain HW-Spaceport)


  (:objects
      
      ;Crew members
      Lewis - Captain
      Sam - Navigator
      Hudson - Engineer
      Kamil - Engineer
      Saad - Science-officer

      ;Vehicles
      MAV-I - MAV
      Probe-I - Probe
      Lander-I - Lander
      
      ;Locations in space
      Mars - Planet
      Pluto - Planet
      Earth - Planet
      Void - Empty
      Medusa - Nebula
      Ceres - Asteroid-belt
      EarthSpaceport - Spaceport 

      ;Space matter
      Medusa-plasma - Plasma
      MarsTDL - touchDownLocation ;TDL (touch down loaction)
      PlutoTDL - touchDownLocation
      PlutoWater - Water
      MarsScan - planetScan
      PlutoScan - planetScan

  )

  (:init
  	  ;Creating pathways around ship 
      (path bridge bridge-sciencelab-airlock)
      (path bridge-sciencelab-airlock bridge)
      (path science-lab bridge-sciencelab-airlock)
      (path bridge-sciencelab-airlock science-lab)

      (path science-lab sciencelab-launchbay-airlock)
      (path sciencelab-launchbay-airlock science-lab)
      (path launch-bay sciencelab-launchbay-airlock)
      (path sciencelab-launchbay-airlock launch-bay)

      (path science-lab sciencelab-engineeringfactory-airlock)
      (path sciencelab-engineeringfactory-airlock science-lab)
      (path engineering-factory sciencelab-engineeringfactory-airlock)
      (path sciencelab-engineeringfactory-airlock engineering-factory)

      ;Ship starting location 
      (ShipOrbiting Earth)

      ;Vehicles start inside the launch bay
      (insideShip MAV-I launch-bay)
      (insideShip Probe-I launch-bay)
      (insideShip Lander-I launch-bay)
      ;(insideShip Lander-II launch-bay)

      ;Crew members starting location 
      (crewMemberLocation engineering-factory Lewis)
      (crewMemberLocation launch-bay Sam)
      (crewMemberLocation launch-bay Hudson)
      (crewMemberLocation launch-bay Kamil)
      (crewMemberLocation bridge Saad)

  )

  (:goal
      (and
      ;Goal

      ;Show cases if a MAV is deployed in a region of space with a nebula then the MAV is disabled 
      (MAVDisabled MAV-I)


      
      )
  )
)
