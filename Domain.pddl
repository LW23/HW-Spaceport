;
; Lewis Wilson, AI coursework 
; LW52 - H00270128
;
(define (domain HW-Spaceport)
    (:requirements
    :strips
    :typing
    :equality
    :adl
    :conditional-effects
    )

    (:types 
            Captain Navigator Engineer Science-officer - Crew
            MAV Probe Lander - Vehicle 
            Planet Empty Nebula Asteroid-belt Spaceport - Region 
            planetScan touchDownLocation Plasma Water - Data
            Bridge Engineering-factory Launch-bay Science-lab Airlock - Rooms
    )
    
    (:constants 
                ;Constants being used to represent rooms/airlocks on ship
                bridge - Bridge
                engineering-factory - Engineering-factory 
                launch-bay - Launch-bay
                science-lab - Science-lab
                bridge-sciencelab-airlock - Airlock
                sciencelab-launchbay-airlock - Airlock
                sciencelab-engineeringfactory-airlock - Airlock

                monitorMAV;Representing location inside engineering factory 
                insideMAV;Representing location inside MAV
    )

    (:predicates
        ;Crew related predicates
        (crewMemberLocation ?location - Rooms ?person - Crew);Crew members location
        (path ?startLocation ?endLocation - Rooms);A path that can be used to move around the ship
        (travelOrders); Captain gives travel orders to navigator
        (transportingPlasma ?scientist - Science-officer);Science officers can pick up the plasma to transport it  
        (StudyPlasma ?plasma - Plasma);Study the collected plasma sample

        ;ship predicates
        (shipDamaged);The ship is damaged
        (ShipOrbiting ?location - Region);The ship is orbiting the location
        (inShipComputer ?data - Data); Data gathered stored in ships computer
        
        ;MAV predicates
        (MAVOrbitingShip ?MAV - MAV ?engineer - Engineer ?location - Region);The MAV is orbiting the ships location
        ;(loadedMAV ?MAV - MAV ?engineer - Engineer);MAV is loaded with an engineer
        ;(unloadedMAV ?MAV - MAV ?engineer - Engineer);Engineer unloaded from the MAV
        (MAVDisabled ?MAV - MAV);MAV is disabled 

        ;Probe predicates
        (ProbeOrbitingShip ?probe - Probe ?location - Region);The probe is orbiting the ships location
        (collectedPlasma ?plasma - Plasma);Probe has colleced plasma
        (transferPlasma ?plasma - Plasma ?probe - Probe);Transfer plasma from the probe to the ships launch bay
        (optimalTDL ?location - touchDownLocation);Probe has found an optimal touch down location (TDL)
        (probeDestroyed ?probe - Probe);Probe is destryoed

        ;Lander predicates
        (landerOrbitingShip ?lander - Lander ?location - Region);The lander is orbiting the ships location 
        (landed ?lander - Lander ?planet - Planet);Lander has landed on the planet
        (ScanPlanet ?planet - Planet);lander scans the planet
        (deployAntenna ?lander - Lander);Deploys antenna 
        (deployHighgainAntenaa ?lander - Lander);Deploys high gain antenna if the planet has a high level of radiation
        (landerCollectedWater ?water - Water);
        (used ?lander - Lander);Lander is used (It has landed on a plaent)
        (waterProcessed ?water - Water);Lander processes

        ;General vehicle predicates
        (operateLaunchControls ?launcher - Engineer);Engineer operates launch controls
        (operateLandingControls ?lander - Engineer);Engineer operates landing controls
        (monitorOperation ?engineer - Engineer);Engineer monitors EVA
        (landedSafley ?vehicle - Vehicle);Vehicle landed safley
        (insideShip ?vehicle - Vehicle ?location - launch-bay);Vehicle back inside ship
        (readyForLaunch ?vehicle - Vehicle);Vehicle is ready to be launched into space

        ;space matter
        (plasmaAtLocation ?plasma - Plasma ?location - Nebula);plasma at this Nebula
        (scanPossibleAtLocation ?scan - planetScan ?location - Planet); Scan possible on this planet
        (TouchDownLocationAtLocation ?TDL - touchDownLocation ?location - Planet);optimal touch down location at this planet
        (planetHighLevelOfRadiation ?planet - Planet);Planet has a high level of radiation 
        (waterOnPlanet ?water - Water ?planet - Planet); Water exists on this planet
        
        ;Spaceport predicates
        (dockedSpacePort ?spaceport - Region);Ship docked at spaceport 
        (transferToSpacePort ?data - Data);Transfer mission results to spaceport from ship
        (transferToMissionControl ?data - Data) ;Transfer mission results to mission control from spaceport   
    )

; Travel
; Precondition :The ship is orbiting the starting location, the navigator is on the bridge
;               the ship is not damaged, the navigator has recived travel orders and all vehicles
;               are inside the ship
; Effect       :The ship is no longer at the start location and is now at the end location  

(:action Travel
    :parameters
        (?startLocation ?endLocation - Region)
    :precondition
        (and
           (ShipOrbiting ?startLocation)
           (exists (?x - Navigator)(crewMemberLocation bridge ?x))
           (not(shipDamaged))
           (travelOrders)

           ;Makes sure all vehicles are inside the ship before travelling
           (exists (?mav - MAV)(insideShip ?mav launch-bay))
           (exists (?probe - Probe)(insideShip ?probe launch-bay))
           (exists (?lander - Lander)(insideShip ?lander launch-bay))
        )
    :effect
        (and
           (not(ShipOrbiting ?startLocation))
           (ShipOrbiting ?endLocation)
        )
)

; Travel Orders
; Precondition :Captain and navigator are both on the bridge 
; Effect       :Captain gives the navigator travel orders

(:action travelOrders
    :parameters
        ()
    :precondition
        (and
           (exists (?x - Captain)(crewMemberLocation bridge ?x))
           (exists (?x - Navigator)(crewMemberLocation bridge ?x))
     	)
    :effect
        (and
           (travelOrders)
        )
)

; Move crew member  
; Precondition :Crew member at start location and the path crew member is taking
; Effect       :Crew member not at start location now at end location

(:action moveCrewMember
    :parameters
        (?person - Crew ?startLocation ?endLocation - Rooms)
    :precondition
        (and
            (crewMemberLocation ?startLocation ?person)
            (path ?startLocation ?endLocation)
        )
    :effect
        (and
            (not (crewMemberLocation ?startLocation ?person))
            (crewMemberLocation ?endLocation ?person)
        )
)

; Load MAV 
; Precondition :An engineer is at the launch bay, the MAV is inside the ship and 
;               the engineer to be loaded is not operating the launch controls
; Effect       :Crew member is now loaded inside the MAV and is no longer in the launch-bay
;               (MAV is still inside the launch bay but engineers location is said 
;               to be inside-MAV) 

(:action LoadMAV
    :parameters
        (?crewToBeLoaded - Engineer ?MAV - MAV)
    :precondition
        (and
        	(crewMemberLocation launch-bay ?crewToBeLoaded)
            (insideShip ?MAV launch-bay)  
            ;Don't load the crew member who is operating the launch controls
            (not(operateLaunchControls ?crewToBeLoaded))
        )
    :effect
        (and
            (crewMemberLocation insideMAV ?crewToBeLoaded)
            (not(crewMemberLocation launch-bay ?crewToBeLoaded))
        )
)

; Operate Launch Controls 
; Precondition : An engineer is at the launch bay
; Effect       : Engineer at the launch bay is operating the launch controls 

(:action operateLaunchControls
    :parameters
        (?launcher - Engineer)
    :precondition
        (and
            (crewMemberLocation launch-bay ?launcher)
        )
    :effect
        (and
            (operateLaunchControls ?launcher) 
        )
)

; Launch MAV 
; Precondition :MAV is Engineer inside MAV, engineer operating launch controls and ship
;              :orbiting at current location 
; Effect       :MAV orbiting ship at the ships current location and MAV is no longer inside
;               the ship.

(:action launchMAV
    :parameters
        (?launcher ?engineer - Engineer  ?MAV - MAV ?location - Region)
    :precondition
        (and
            (crewMemberLocation insideMAV ?engineer)
            (crewMemberLocation launch-bay ?launcher)
            (operateLaunchControls ?launcher)
            (ShipOrbiting ?location)
            
        )
    :effect
        (and
            (MAVOrbitingShip ?mav ?engineer ?location)
            (not(insideShip ?mav launch-bay))
            
        )
)

; Land MAV
; Precondition :MAV is orbiting the ship and an engineer is in the launch bay to retrieve
;               the MAV
; Effect       :MAV is not orbiting the ship has has safley landed in the launch bay

(:action landMAV
    :parameters
        (?lander ?engineer - Engineer  ?mav - MAV ?location - Region)
    :precondition
        (and
            (MAVOrbitingShip ?mav ?engineer ?location)
            (crewMemberLocation launch-bay ?lander) 
        )
    :effect
        (and
            (not(MAVOrbitingShip ?mav ?engineer ?location))
            (insideShip ?mav launch-bay)
            (landedSafley ?mav)
        )
)

; Unload MAV
; Precondition :The MAV is loaded with an engineer and the mav has safley landed
;               in the launch bay 
; Effect       :The engineer is unloaded from the MAV and is back in the launch bay

(:action unloadMAV
    :parameters
        (?crewToBeUnloaded - Engineer ?mav - MAV)
    :precondition
        (and
            (crewMemberLocation insideMAV ?crewToBeUnloaded)
            (insideShip ?mav launch-bay)
            (landedSafley ?mav)
        )
    :effect
        (and
            (not(crewMemberLocation insideMAV ?crewToBeUnloaded))
            (crewMemberLocation launch-bay ?crewToBeUnloaded)
            
        )
)

; Monitor Operation 
; Precondition :An engineer is at the engineering factory and a mav is orbiting the ship
; Effect       :The engineer monitors the MAVs operation

(:action monitorOperation
    :parameters
        (?engineer - Engineer);?engInSpace - Engineer ?mav - MAV)
    :precondition
        (and
            (crewMemberLocation engineering-factory ?engineer)
            (exists (?mav - MAV ?engInSpace - Engineer ?location - Region)(MAVOrbitingShip ?mav ?engInSpace ?location))
        )
    :effect
        (and
            (crewMemberLocation monitorMAV ?engineer)
        )
)

; Perform EVA
; Precondition :The ship is damaged, a MAV is orbiting the ship and a crew member is 
;               monitoring the operation
; Effect       :The ship is repaired and the engineer is no longer monitoring the operation

(:action performEVA
    :parameters
        (?mav - MAV ?engineerInSpace ?engineer - Engineer ?location - Region)
    :precondition
        (and
            (shipDamaged)
            (MAVOrbitingShip ?mav ?engineerInSpace ?location)
            (crewMemberLocation monitorMAV ?engineer)
        )
    :effect
        (and
            (not(shipDamaged))
            (not(monitorOperation ?engineer))
            (crewMemberLocation engineering-factory ?engineer)            
        )
)

; Launch probe
; Precondition :Probe is inside the ship, engineer is opearting the launch controls and ship
;               orbiting at current location 
; Effect       :Probe orbiting ship at the ships current location and probe is no longer inside
;               the ship

(:action launchProbe
    :parameters
        (?probe - Probe ?launcher - Engineer ?location - Region)
    :precondition
        (and
            (insideShip ?probe launch-bay)
            (operateLaunchControls ?launcher) 
            (ShipOrbiting ?location)
        )
    :effect
        (and
            (ProbeOrbitingShip ?probe ?location)
            (not(insideShip ?probe launch-bay))     
        )
)

; Land probe
; Precondition :Probe is orbiting the ship and an engineer is in the launch bay to retrieve
;               the probe 
; Effect       :Probe is not orbiting the ship has has safley landed in the launch bay

(:action landProbe
    :parameters
        (?probe - Probe ?lander - Engineer ?location - Region)
    :precondition
        (and
            (ProbeOrbitingShip ?probe ?location)
            (crewMemberLocation launch-bay ?lander)       
        )
    :effect
        (and
            (not(ProbeOrbitingShip ?probe ?location))
            (insideShip ?probe launch-bay)
            (landedSafley ?probe)
        )
)

; Collect Plasma
; Precondition :Probe orbiting at the ships current location and plasma exists at this location
; Effect       :Probe has collected a plasma sample

(:action collectPlasma
    :parameters
        (?probe - Probe ?plasma - Plasma ?location - Nebula)
    :precondition
        (and
          (ShipOrbiting ?location)
          (ProbeOrbitingShip ?probe ?location)
          (plasmaAtLocation ?plasma ?location);makes sure we are working with what we expect
        )
    :effect
        (and
          (collectedPlasma ?plasma) ;?location)
        )
)

; Unload plasma 
; Precondition :Probe has landed safley and has collected a plasma sample
; Effect       :Transfers plasma sample to ships launch bay

(:action unloadPlasma
    :parameters
        (?plasma - Plasma ?probe - Probe)
    :precondition
        (and
            (landedSafley ?probe)
            (collectedPlasma ?plasma)      
        )
    :effect
        (and
            (transferPlasma ?plasma ?probe)
        )
)

; Pick up plasma 
; Precondition :Plasma has been transferred to the ship and a science officer is at the launch bay
; Effect       :Science officer is transporting a plasma sample 

(:action pickUpPlasma 
    :parameters
        (?scientist - Science-officer ?plasma - Plasma)
    :precondition
        (and
            (exists(?probe - Probe)(transferPlasma ?plasma ?probe))
            (crewMemberLocation launch-bay ?scientist)
        )
    :effect
        (and
            (transportingPlasma ?scientist)
        )
)

; Study Plasma 
; Precondition :Science officer is in the science lab and is transporting a plasma sample
; Effect       :Science officer is no longer transporting plasma sample and the sample is 
;               studied and stored in the ships computer 

(:action StudyPlasma 
    :parameters
        (?scientist - Science-officer ?plasma - Plasma)
    :precondition
        (and
            (crewMemberLocation science-lab ?scientist)
            (transportingPlasma ?scientist)
        )
    :effect
        (and
            (inShipComputer ?plasma)
            (not(transportingPlasma ?scientist))
        )
)

; Find optimal touch down location (TDL)
; Precondition :Ship is orbiting at current location, probe orbiting ship and an optimal
;               touch down location exists at the location
;       Effect :Probe has found an optimal TDL

(:action findOptimalTDL
    :parameters
        (?probe - Probe ?TDL - touchDownLocation ?location - Planet)
    :precondition
        (and
          (ShipOrbiting ?location);makes sure ship is in correct space region
          (ProbeOrbitingShip ?probe ?location)
          (TouchDownLocationAtLocation ?TDL ?location)
        )
    :effect
        (and
          (optimalTDL ?TDL) ;?location)
        )
)

; Transfer touch down location (TDL) 
; Precondition : Probe has landed safley and an optimal touch down location was found
; Effect       : TDL now stored in ships computer 

(:action transferTDL
    :parameters
        (?TDL - touchDownLocation ?probe - Probe)
    :precondition
        (and
            (landedSafley ?probe)
            (optimalTDL ?TDL)
        )
    :effect
        (and
            (inShipComputer ?TDL)
        )
)

; Launch lander
; Precondition :Lander is inside the ship, engineer is opearting the launch controls, ship
;               orbiting at current location and the TDL is inside the ships computer
; Effect       :Lander orbiting ship at the ships current location and lander is no longer inside
;               the ship

(:action launchLander
    :parameters
        (?lander - Lander ?launcher - Engineer ?TDL - touchDownLocation ?location - Region)
    :precondition
        (and
            (ShipOrbiting ?location)
            (insideShip ?lander launch-bay)
            (operateLaunchControls ?launcher) 
            (inShipComputer ?TDL); Need our landing destination
        )
    :effect
        (and
            (landerOrbitingShip ?lander ?location)
            (not(insideShip ?lander launch-bay))
        )
)

; Land on planet
; Precondition :Lander orbiting ship at the ships current location, TDL exists at current 
;               planet and the TDL is stored in the ships computer              
; Effect       :Lander has landed safley on the plaent and is no longer orbiting the ship.
;               The lander has been used

(:action landOnPlanet
    :parameters
        (?lander - Lander ?TDL - touchDownLocation ?planet - Planet ?location - Region)
    :precondition
        (and
            (landerOrbitingShip ?lander ?location)
            (TouchDownLocationAtLocation ?TDL ?planet)
            (inShipComputer ?TDL)
            (not(used ?lander))
        )
    :effect
        (and
            (landed ?lander ?planet)
            (not(landerOrbitingShip ?lander ?location))
            (used ?lander)
            (landedSafley ?lander)
        )
)

; Scan planet 
; Precondition :Lander has landed on the plaent and scan possible at the location
; Effect       :Lander scans planet


(:action scanPlanet 
    :parameters
        (?lander - Lander ?planet - Planet ?scan - planetScan)
    :precondition
        (and
            (landed ?lander ?planet)
            (scanPossibleAtLocation ?scan ?planet)
        )
    :effect
        (and
            (ScanPlanet ?planet)
        )
)

; Transmit plaent scan results  
; Precondition :Lander has scanned the planet and the plaent does not have a high level
;               level of radiation
; Effect       :Lander deploys Antenna and the result of the scan are stored in the ships 
;               computer

(:action transmitPlanetScanResults 
    :parameters
        (?lander - Lander ?planet - Planet ?scan - planetScan )
    :precondition
        (and
            (ScanPlanet ?planet)
            (not(planetHighLevelOfRadiation ?planet))
        )
    :effect
        (and
            (deployAntenna ?lander)
            (inShipComputer ?scan)
        )
)

; Transmit plaent scan results high radiation
; Precondition :Lander has scanned the planet and the plaent does have a high level
;               level of radiation
; Effect       :Lander deploys high gain antenaa and the result of the scan are stored in the ships 
;               computer

(:action transmitPlanetScanResultsHighRadiation 
    :parameters
        (?lander - Lander ?planet - Planet ?scan - planetScan )
    :precondition
        (and
            (ScanPlanet ?planet)
            (planetHighLevelOfRadiation ?planet)
        )
    :effect
        (and
            (deployHighgainAntenaa ?lander)
            (inShipComputer ?scan)
        )
)

; Ship in asteroid belt
; Precondition :Ship orbting at a loccation with a asteroid belt
; Effect       :Ship becomes damaged

(:action shipInAsteroidBelt  
    :parameters
        (?location - Asteroid-belt)
    :precondition
        (and
            (ShipOrbiting ?location)  
        )
    :effect
        (and
            (shipDamaged)
        )
)

; Probe destroyed 
; Precondition :Probe orbiting ship at a location wtih a asteroid belt
; Effect       :Probe destroyed

(:action probeDestroyed  
    :parameters
        (?probe - Probe ?location - Asteroid-belt)
    :precondition
        (and
           (ProbeOrbitingShip ?probe ?location) 
        )
    :effect
        (and
            (probeDestroyed ?probe)
        )
)

; MAV disabled 
; Precondition :MAV orbiting ship at location with a nebula
; Effect       :MAV is disabled

(:action MAVDisabled  
    :parameters
        (?MAV - MAV ?engineer - Engineer ?location - Nebula)
    :precondition
        (and
           (MAVOrbitingShip ?MAV ?engineer ?location) 
        )
    :effect
        (and
            (MAVDisabled ?MAV)
        )
)

; Return to space  
; Precondition :Data from mission is in the ship computer 
; Effect       :Ship is docked to space port and is transfering data to the space port

(:action returnToSpaceport  
    :parameters
        (?data - Data ?location - Spaceport)
    :precondition
        (and
            (inShipComputer ?data)
        )
    :effect
        (and
            (dockedSpacePort ?location)
            (transferToSpacePort ?data)
        )
)

; Transfer to mission control
; Precondition :Ship is transfering data to space port
; Effect       :Data is no longer on the ships computer and is transferred to mission control
;              :from the space port

(:action transferToMissionControl  
    :parameters
        (?data - Data )
    :precondition
        (and
            (transferToSpacePort ?data)
        )
    :effect
        (and
            (not(inShipComputer ?data))
            (transferToMissionControl ?data)
        )
)

; Search for water
; Precondition :Lander has landed on the planet and water exists on the planet
; Effect       :Lander collects water sample

(:action searchForWater 
    :parameters
        (?lander - Lander ?water - Water ?planet - Planet)
    :precondition
        (and
            (landed ?lander ?planet)
            (waterOnPlanet ?water ?planet)
            ;(ShipOrbiting ?planet)
        )
    :effect
        (and
            (landerCollectedWater ?water)
        )
)

; Process water 
; Precondition :Lander collected a water sample
; Effect       :Lander procsses water  

(:action processWater 
    :parameters
        (?water - Water)
    :precondition
        (and
            (landerCollectedWater ?water)
        )
    :effect
        (and
            (waterProcessed ?water)
        )
)

; Transmit water processing results  
; Precondition :Water has been processed and the ship is orbiting at the plaent 
; Effect       :Lander deploys Antenna and the result of the water processing are stored 
;               in the ships computer
          
(:action transmitWaterProcessingResults 
    :parameters
        (?lander - Lander ?planet - Planet ?water - Water )
    :precondition
        (and
            (waterProcessed ?water)
            (ShipOrbiting ?planet)
        )
    :effect
        (and
            (deployAntenna ?lander)
            (inShipComputer ?water)
        )
)
)


